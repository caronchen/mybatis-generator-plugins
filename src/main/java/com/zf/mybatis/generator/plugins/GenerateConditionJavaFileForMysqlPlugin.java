package com.zf.mybatis.generator.plugins;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;

/**
 * 用于Mysql生成分页语句
 *
 * @author zhifeng
 */
public class GenerateConditionJavaFileForMysqlPlugin extends GenerateConditionJavaFilePlugin {

    @Override
    protected void addPaginationSupportXmlElement(XmlElement answer, IntrospectedTable introspectedTable) {
        XmlElement ifElement = new XmlElement("if");
        ifElement.addAttribute(TEST_ATTRIBUTE_WITHIN_PAGE);
        ifElement.addElement(new TextElement("LIMIT #{pageOffset,jdbcType=INTEGER}, #{pageSize,jdbcType=INTEGER}"));
        answer.addElement(ifElement);
    }
}
