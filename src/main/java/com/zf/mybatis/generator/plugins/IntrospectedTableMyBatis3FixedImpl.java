package com.zf.mybatis.generator.plugins;

import java.util.ArrayList;
import java.util.List;
import org.mybatis.generator.api.CommentGenerator;
import org.mybatis.generator.api.FullyQualifiedTable;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.ProgressCallback;
import org.mybatis.generator.api.dom.java.CompilationUnit;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.InitializationBlock;
import org.mybatis.generator.api.dom.java.InnerClass;
import org.mybatis.generator.api.dom.java.JavaVisibility;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.Parameter;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;
import org.mybatis.generator.codegen.AbstractJavaClientGenerator;
import org.mybatis.generator.codegen.AbstractJavaGenerator;
import org.mybatis.generator.codegen.AbstractXmlGenerator;
import org.mybatis.generator.codegen.mybatis3.IntrospectedTableMyBatis3Impl;
import org.mybatis.generator.codegen.mybatis3.MyBatis3FormattingUtilities;
import org.mybatis.generator.codegen.mybatis3.model.BaseRecordGenerator;
import org.mybatis.generator.codegen.mybatis3.model.PrimaryKeyGenerator;
import org.mybatis.generator.codegen.mybatis3.model.RecordWithBLOBsGenerator;
import org.mybatis.generator.codegen.mybatis3.xmlmapper.XMLMapperGenerator;
import org.mybatis.generator.codegen.mybatis3.xmlmapper.elements.AbstractXmlElementGenerator;
import static org.mybatis.generator.internal.util.StringUtility.stringHasValue;
import static org.mybatis.generator.internal.util.messages.Messages.getString;

/**
 * {@linkplain FixedExampleGenerator} 支持，用于生成抽象的查询条件类
 *
 * @author zhifeng
 */
public class IntrospectedTableMyBatis3FixedImpl extends IntrospectedTableMyBatis3Impl {

    public static final class FixedExampleGenerator extends AbstractJavaGenerator {

        @Override
        public List<CompilationUnit> getCompilationUnits() {
            FullyQualifiedTable table = introspectedTable.getFullyQualifiedTable();
            progressCallback.startTask(getString("Progress.9", table.toString())); //$NON-NLS-1$
            CommentGenerator commentGenerator = context.getCommentGenerator();

            FullyQualifiedJavaType type = new FullyQualifiedJavaType(introspectedTable.getExampleType());
            TopLevelClass topLevelClass = new TopLevelClass(type);
            topLevelClass.addImportedType("java.util.Map");
            topLevelClass.addImportedType("java.util.HashMap");
            topLevelClass.setVisibility(JavaVisibility.PUBLIC);
            commentGenerator.addJavaFileComment(topLevelClass);


            Field columnMappingField = new Field();
            columnMappingField.setName("FIELD_COLUMN_MAPPING");
            columnMappingField.setVisibility(JavaVisibility.PRIVATE);
            columnMappingField.setStatic(true);
            columnMappingField.setFinal(true);
            columnMappingField.setType(new FullyQualifiedJavaType("Map<String, String>"));
            columnMappingField.setInitializationString("new HashMap<String, String>()");
            topLevelClass.addField(columnMappingField);

            topLevelClass.addInitializationBlock(getInitializationBlock());

            Method method = new Method();
            method.addAnnotation("@Override");
            method.setVisibility(JavaVisibility.PROTECTED);
            method.setName("createCriteriaInternal"); //$NON-NLS-1$
            method.setReturnType(FullyQualifiedJavaType.getCriteriaInstance());
            method.addBodyLine("return new Criteria();"); //$NON-NLS-1$
            topLevelClass.addMethod(method);

            method = new Method();
            method.addAnnotation("@Override");
            method.setVisibility(JavaVisibility.PUBLIC);
            method.setName("getColumnMapping"); //$NON-NLS-1$
            method.setReturnType(new FullyQualifiedJavaType("Map<String, String>"));
            method.addBodyLine("return FIELD_COLUMN_MAPPING;"); //$NON-NLS-1$
            topLevelClass.addMethod(method);

            topLevelClass.addInnerClass(getCriteriaInnerClass(topLevelClass));

            List<CompilationUnit> answer = new ArrayList<CompilationUnit>();
            if (context.getPlugins().modelExampleClassGenerated(
                    topLevelClass, introspectedTable)) {
                answer.add(topLevelClass);
            }
            return answer;
        }

        private InitializationBlock getInitializationBlock() {
            InitializationBlock block = new InitializationBlock();
            block.setStatic(true);

            StringBuilder sb = new StringBuilder();
            for (IntrospectedColumn introspectedColumn : introspectedTable.getNonBLOBColumns()) {
                sb.append("FIELD_COLUMN_MAPPING.put(\""); //$NON-NLS-1$
                sb.append(introspectedColumn.getJavaProperty());
                sb.append("\", \""); //$NON-NLS-1$
                sb.append(MyBatis3FormattingUtilities.getAliasedActualColumnName(introspectedColumn));
                sb.append("\");");

                block.addBodyLine(sb.toString());

                sb.setLength(0);
            }

            return block;
        }

        private InnerClass getCriteriaInnerClass(TopLevelClass topLevelClass) {
            InnerClass answer = new InnerClass(FullyQualifiedJavaType.getCriteriaInstance());

            answer.setVisibility(JavaVisibility.PUBLIC);
            answer.setStatic(true);
            answer.setSuperClass(new FullyQualifiedJavaType("AbstractCriteria"));

            context.getCommentGenerator().addClassComment(answer, introspectedTable, true);

            Method getCriteriaMethod = new Method();
            getCriteriaMethod.addAnnotation("@Override");
            getCriteriaMethod.setVisibility(JavaVisibility.PROTECTED);
            getCriteriaMethod.setName("getCriteria"); //$NON-NLS-1$
            getCriteriaMethod.setReturnType(new FullyQualifiedJavaType("java.util.List<Criterion>")); //$NON-NLS-1$
            getCriteriaMethod.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "typeHandler")); //$NON-NLS-1$

            Method getAllCriteriaMethod = new Method();
            getAllCriteriaMethod.addAnnotation("@Override");
            getAllCriteriaMethod.setVisibility(JavaVisibility.PUBLIC);
            getAllCriteriaMethod.setName("getAllCriteria"); //$NON-NLS-1$
            getAllCriteriaMethod.setReturnType(new FullyQualifiedJavaType("java.util.List<Criterion>")); //$NON-NLS-1$

            // now generate the criteria field and getter for typehandler filed.
            boolean hasTypeHandlerFiled = false;
            for (IntrospectedColumn introspectedColumn : introspectedTable.getNonBLOBColumns()) {
                if (stringHasValue(introspectedColumn.getTypeHandler())) {
                    hasTypeHandlerFiled = true;

                    Field field = new Field();
                    field.setName(introspectedColumn.getJavaProperty() + "Criteria"); //$NON-NLS-1$
                    field.setFinal(true);
                    field.setType(new FullyQualifiedJavaType("List<Criterion>")); //$NON-NLS-1$
                    field.setInitializationString("new ArrayList<Criterion>()");
                    field.setVisibility(JavaVisibility.PRIVATE);
                    answer.addField(field);
                    answer.addMethod(getGetter(field));

                    getCriteriaMethod.addBodyLine("if (\"" + introspectedColumn.getTypeHandler() + "\".equals(typeHandler)) {"); //$NON-NLS-1$
                    getCriteriaMethod.addBodyLine("return " + field.getName() + ";"); //$NON-NLS-1$
                    getCriteriaMethod.addBodyLine("}"); //$NON-NLS-1$

                    getAllCriteriaMethod.addBodyLine("criteriaList.addAll(" + field.getName() + ");"); //$NON-NLS-1$
                }
            }
            if (hasTypeHandlerFiled) {
                topLevelClass.addImportedType(FullyQualifiedJavaType.getNewListInstance());
                topLevelClass.addImportedType(FullyQualifiedJavaType.getNewArrayListInstance());

                getCriteriaMethod.addBodyLine("return super.getCriteria(typeHandler);"); //$NON-NLS-1$
                answer.addMethod(getCriteriaMethod);

                getAllCriteriaMethod.addBodyLine(0, "List<Criterion> criteriaList = super.getAllCriteria();"); //$NON-NLS-1$
                getAllCriteriaMethod.addBodyLine("return criteriaList;"); //$NON-NLS-1$
                answer.addMethod(getAllCriteriaMethod);
            }

            Method method = new Method();
            method.setVisibility(JavaVisibility.PROTECTED);
            method.setName("Criteria"); //$NON-NLS-1$
            method.setConstructor(true);
            method.addBodyLine("super();"); //$NON-NLS-1$
            answer.addMethod(method);

            for (IntrospectedColumn introspectedColumn : introspectedTable.getNonBLOBColumns()) {
                topLevelClass.addImportedType(introspectedColumn.getFullyQualifiedJavaType());
                answer.addMethod(getAndMethod(introspectedColumn));
            }

            return answer;
        }

        private Method getAndMethod(IntrospectedColumn introspectedColumn) {
            Method method = new Method();
            method.setVisibility(JavaVisibility.PUBLIC);

            StringBuilder sb = new StringBuilder();
            sb.append(introspectedColumn.getJavaProperty());
            sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
            sb.insert(0, "and"); //$NON-NLS-1$
            method.setName(sb.toString());

            boolean isString = "java.lang.String".equals(introspectedColumn.getFullyQualifiedJavaType().getFullyQualifiedName())
                    || "String".equals(introspectedColumn.getFullyQualifiedJavaType().getFullyQualifiedName());
            sb.setLength(0);
            if (isString) {
                sb.append("CharSequenceCriteriaAssertion<Criteria>");
            } else {
                sb.append("CriteriaAssertion<Criteria, "); //$NON-NLS-1$
                sb.append(introspectedColumn.getFullyQualifiedJavaType());
                sb.append(">"); //$NON-NLS-1$
            }
            method.setReturnType(new FullyQualifiedJavaType(sb.toString()));

            sb.setLength(0);
            if (isString) {
                sb.append("return new CharSequenceCriteriaAssertion<Criteria");
            } else {
                sb.append("return new CriteriaAssertion<Criteria, "); //$NON-NLS-1$
                sb.append(introspectedColumn.getFullyQualifiedJavaType());
            }
            sb.append(">(this, \""); //$NON-NLS-1$
            sb.append(MyBatis3FormattingUtilities.getAliasedActualColumnName(introspectedColumn));
            sb.append("\", \""); //$NON-NLS-1$
            sb.append(introspectedColumn.getJavaProperty());
            if (stringHasValue(introspectedColumn.getTypeHandler())) {
                sb.append("\", \""); //$NON-NLS-1$
                sb.append(introspectedColumn.getTypeHandler());
            }
            sb.append("\");"); //$NON-NLS-1$
            method.addBodyLine(sb.toString());

            return method;
        }
    }

    @Override
    protected void calculateJavaModelGenerators(List<String> warnings, ProgressCallback progressCallback) {
        if (getRules().generateExampleClass()) {
            AbstractJavaGenerator javaGenerator = new FixedExampleGenerator();
            initializeAbstractGenerator(javaGenerator, warnings,
                    progressCallback);
            javaModelGenerators.add(javaGenerator);
        }

        if (getRules().generatePrimaryKeyClass()) {
            AbstractJavaGenerator javaGenerator = new PrimaryKeyGenerator();
            initializeAbstractGenerator(javaGenerator, warnings,
                    progressCallback);
            javaModelGenerators.add(javaGenerator);
        }

        if (getRules().generateBaseRecordClass()) {
            AbstractJavaGenerator javaGenerator = new BaseRecordGenerator();
            initializeAbstractGenerator(javaGenerator, warnings,
                    progressCallback);
            javaModelGenerators.add(javaGenerator);
        }

        if (getRules().generateRecordWithBLOBsClass()) {
            AbstractJavaGenerator javaGenerator = new RecordWithBLOBsGenerator();
            initializeAbstractGenerator(javaGenerator, warnings,
                    progressCallback);
            javaModelGenerators.add(javaGenerator);
        }
    }

    @Override
    protected void calculateXmlMapperGenerator(AbstractJavaClientGenerator javaClientGenerator, List<String> warnings, ProgressCallback progressCallback) {
        super.calculateXmlMapperGenerator(javaClientGenerator, warnings, progressCallback);

        if (xmlMapperGenerator != null && xmlMapperGenerator instanceof XMLMapperGenerator) {
            xmlMapperGenerator = wrapXmlMapperGenerator(xmlMapperGenerator);
        }
    }

    private AbstractXmlGenerator wrapXmlMapperGenerator(AbstractXmlGenerator xmlGenerator) {
        XMLMapperGenerator generator = new XMLMapperGenerator() {

            @Override
            protected void addSelectByExampleWithBLOBsElement(XmlElement parentElement) {
                if (introspectedTable.getRules().generateSelectByExampleWithBLOBs()) {
                    AbstractXmlElementGenerator elementGenerator = new CustomSelectByExampleWithBLOBsElementGenerator();
                    initializeAndExecuteGenerator(elementGenerator, parentElement);
                }
            }

            @Override
            protected void addSelectByExampleWithoutBLOBsElement(XmlElement parentElement) {
                if (introspectedTable.getRules().generateSelectByExampleWithoutBLOBs()) {
                    AbstractXmlElementGenerator elementGenerator = new CustomSelectByExampleWithoutBLOBsElementGenerator();
                    initializeAndExecuteGenerator(elementGenerator, parentElement);
                }
            }

            @Override
            protected void addCountByExampleElement(XmlElement parentElement) {
                if (introspectedTable.getRules().generateCountByExample()) {
                    AbstractXmlElementGenerator elementGenerator = new CustomCountByExampleElementGenerator();
                    initializeAndExecuteGenerator(elementGenerator, parentElement);
                }
            }

        };
        generator.setContext(xmlGenerator.getContext());
        generator.setIntrospectedTable(xmlGenerator.getIntrospectedTable());
        generator.setProgressCallback(xmlGenerator.getProgressCallback());
        generator.setWarnings(xmlGenerator.getWarnings());
        return generator;
    }

    public static class CustomSelectByExampleWithBLOBsElementGenerator extends AbstractXmlElementGenerator {

        public CustomSelectByExampleWithBLOBsElementGenerator() {
            super();
        }

        @Override
        public void addElements(XmlElement parentElement) {
            String fqjt = introspectedTable.getExampleType();

            XmlElement answer = new XmlElement("select"); //$NON-NLS-1$
            answer
                    .addAttribute(new Attribute(
                                    "id", introspectedTable.getSelectByExampleWithBLOBsStatementId())); //$NON-NLS-1$
            answer.addAttribute(new Attribute(
                    "resultMap", introspectedTable.getResultMapWithBLOBsId())); //$NON-NLS-1$
            answer.addAttribute(new Attribute("parameterType", fqjt)); //$NON-NLS-1$

            context.getCommentGenerator().addComment(answer);

            answer.addElement(new TextElement("select")); //$NON-NLS-1$
            XmlElement ifElement = new XmlElement("if"); //$NON-NLS-1$
            ifElement.addAttribute(new Attribute("test", "distinct")); //$NON-NLS-1$ //$NON-NLS-2$
            ifElement.addElement(new TextElement("distinct")); //$NON-NLS-1$
            answer.addElement(ifElement);

            XmlElement chooseElement = new XmlElement("choose"); //$NON-NLS-1$

            XmlElement whenElement = new XmlElement("when"); //$NON-NLS-1$
            whenElement.addAttribute(new Attribute("test", "columns != null")); //$NON-NLS-1$ //$NON-NLS-2$
            whenElement.addElement(new TextElement("${columns}")); //$NON-NLS-1$
            chooseElement.addElement(whenElement);

            whenElement = new XmlElement("otherwise"); //$NON-NLS-1$
            StringBuilder sb = new StringBuilder();
            if (stringHasValue(introspectedTable
                    .getSelectByExampleQueryId())) {
                sb.append('\'');
                sb.append(introspectedTable.getSelectByExampleQueryId());
                sb.append("' as QUERYID,"); //$NON-NLS-1$
                whenElement.addElement(new TextElement(sb.toString()));
            }
            whenElement.addElement(getBaseColumnListElement());
            whenElement.addElement(new TextElement(",")); //$NON-NLS-1$
            whenElement.addElement(getBlobColumnListElement());
            chooseElement.addElement(whenElement);

            answer.addElement(chooseElement);

            sb.setLength(0);
            sb.append("from "); //$NON-NLS-1$
            sb.append(introspectedTable
                    .getAliasedFullyQualifiedTableNameAtRuntime());
            answer.addElement(new TextElement(sb.toString()));
            answer.addElement(getExampleIncludeElement());

            ifElement = new XmlElement("if"); //$NON-NLS-1$
            ifElement.addAttribute(new Attribute("test", "orderByClause != null")); //$NON-NLS-1$ //$NON-NLS-2$
            ifElement.addElement(new TextElement("order by ${orderByClause}")); //$NON-NLS-1$
            answer.addElement(ifElement);

            if (context.getPlugins()
                    .sqlMapSelectByExampleWithBLOBsElementGenerated(answer,
                            introspectedTable)) {
                parentElement.addElement(answer);
            }
        }
    }

    public static class CustomSelectByExampleWithoutBLOBsElementGenerator extends AbstractXmlElementGenerator {

        public CustomSelectByExampleWithoutBLOBsElementGenerator() {
            super();
        }

        @Override
        public void addElements(XmlElement parentElement) {
            String fqjt = introspectedTable.getExampleType();

            XmlElement answer = new XmlElement("select"); //$NON-NLS-1$

            answer.addAttribute(new Attribute("id", //$NON-NLS-1$
                    introspectedTable.getSelectByExampleStatementId()));
            answer.addAttribute(new Attribute(
                    "resultMap", introspectedTable.getBaseResultMapId())); //$NON-NLS-1$
            answer.addAttribute(new Attribute("parameterType", fqjt)); //$NON-NLS-1$

            context.getCommentGenerator().addComment(answer);

            answer.addElement(new TextElement("select")); //$NON-NLS-1$
            XmlElement ifElement = new XmlElement("if"); //$NON-NLS-1$
            ifElement.addAttribute(new Attribute("test", "distinct")); //$NON-NLS-1$ //$NON-NLS-2$
            ifElement.addElement(new TextElement("distinct")); //$NON-NLS-1$
            answer.addElement(ifElement);

            XmlElement chooseElement = new XmlElement("choose"); //$NON-NLS-1$

            XmlElement whenElement = new XmlElement("when"); //$NON-NLS-1$
            whenElement.addAttribute(new Attribute("test", "columns != null")); //$NON-NLS-1$ //$NON-NLS-2$
            whenElement.addElement(new TextElement("${columns}")); //$NON-NLS-1$
            chooseElement.addElement(whenElement);

            whenElement = new XmlElement("otherwise"); //$NON-NLS-1$
            StringBuilder sb = new StringBuilder();
            if (stringHasValue(introspectedTable
                    .getSelectByExampleQueryId())) {
                sb.append('\'');
                sb.append(introspectedTable.getSelectByExampleQueryId());
                sb.append("' as QUERYID,"); //$NON-NLS-1$
                whenElement.addElement(new TextElement(sb.toString()));
            }
            whenElement.addElement(getBaseColumnListElement());
            chooseElement.addElement(whenElement);

            answer.addElement(chooseElement);

            sb.setLength(0);
            sb.append("from "); //$NON-NLS-1$
            sb.append(introspectedTable
                    .getAliasedFullyQualifiedTableNameAtRuntime());
            answer.addElement((new TextElement(sb.toString())));
            answer.addElement(getExampleIncludeElement());

            ifElement = new XmlElement("if"); //$NON-NLS-1$
            ifElement.addAttribute(new Attribute("test", "orderByClause != null")); //$NON-NLS-1$ //$NON-NLS-2$
            ifElement.addElement(new TextElement("order by ${orderByClause}")); //$NON-NLS-1$
            answer.addElement(ifElement);

            if (context.getPlugins()
                    .sqlMapSelectByExampleWithoutBLOBsElementGenerated(answer,
                            introspectedTable)) {
                parentElement.addElement(answer);
            }
        }
    }

    public static class CustomCountByExampleElementGenerator extends AbstractXmlElementGenerator {

        public CustomCountByExampleElementGenerator() {
            super();
        }

        @Override
        public void addElements(XmlElement parentElement) {
            XmlElement answer = new XmlElement("select"); //$NON-NLS-1$

            String fqjt = introspectedTable.getExampleType();

            answer.addAttribute(new Attribute("id", introspectedTable.getCountByExampleStatementId())); //$NON-NLS-1$
            answer.addAttribute(new Attribute("parameterType", fqjt)); //$NON-NLS-1$
            answer.addAttribute(new Attribute("resultType", "java.lang.Integer")); //$NON-NLS-1$ //$NON-NLS-2$

            context.getCommentGenerator().addComment(answer);

            answer.addElement(new TextElement("select"));

            XmlElement chooseElement = new XmlElement("choose"); //$NON-NLS-1$

            XmlElement whenElement = new XmlElement("when"); //$NON-NLS-1$
            whenElement.addAttribute(new Attribute("test", "distinct and columns != null")); //$NON-NLS-1$ //$NON-NLS-2$
            whenElement.addElement(new TextElement("count(distinct ${columns})")); //$NON-NLS-1$
            chooseElement.addElement(whenElement);

            whenElement = new XmlElement("otherwise"); //$NON-NLS-1$
            whenElement.addElement(new TextElement("count(*)")); //$NON-NLS-1$
            chooseElement.addElement(whenElement);

            answer.addElement(chooseElement);

            StringBuilder sb = new StringBuilder();
            sb.append("from "); //$NON-NLS-1$
            sb.append(introspectedTable
                    .getAliasedFullyQualifiedTableNameAtRuntime());
            answer.addElement(new TextElement(sb.toString()));
            answer.addElement(getExampleIncludeElement());

            if (context.getPlugins().sqlMapCountByExampleElementGenerated(
                    answer, introspectedTable)) {
                parentElement.addElement(answer);
            }
        }
    }
}
