package com.zf.mybatis.generator.plugins;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import org.mybatis.generator.api.GeneratedJavaFile;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.CompilationUnit;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.InnerClass;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.JavaVisibility;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.Parameter;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.Document;
import org.mybatis.generator.api.dom.xml.XmlElement;
import static org.mybatis.generator.codegen.AbstractJavaGenerator.getGetter;
import org.mybatis.generator.codegen.mybatis3.javamapper.elements.AbstractJavaMapperMethodGenerator;
import org.mybatis.generator.config.PropertyRegistry;
import org.mybatis.generator.internal.util.StringUtility;

/**
 * 生成查询条件对象的父类
 *
 * @author zhifeng
 */
public abstract class GenerateConditionJavaFilePlugin extends PluginAdapter {

    private static final Pattern PATTERN = Pattern.compile("[0-9]+");
    protected static final Attribute TEST_ATTRIBUTE_WITHIN_PAGE = new Attribute("test", "pageOffset >= 0 and pageSize > 0");

    private String rootClass;
    private String classSimpleName;
    private String defaultPageSize = "10";
    private boolean rootClassProvided = true;

    @Override
    public boolean validate(List<String> warnings) {
        return parsePageSize(warnings)
                && parseRootClassProvided(warnings)
                && parseRootClass(warnings);
    }

    private boolean parseRootClass(List<String> warnings) {
        String className = this.properties.getProperty("rootClass");

        boolean valid = StringUtility.stringHasValue(className);

        if (valid) {
            this.rootClass = className;
            this.classSimpleName = getShortClassName(className, "Condition");
        } else {
            warnings.add("Property 'rootClass' is not valid.");
        }

        return valid;
    }

    private boolean parseRootClassProvided(List<String> warnings) {
        String provided = this.properties.getProperty("rootClassProvided");

        boolean valid = StringUtility.stringHasValue(provided)
                && ("true".equalsIgnoreCase(provided) || "false".equalsIgnoreCase(provided));

        if (valid) {
            this.rootClassProvided = Boolean.parseBoolean(provided);
        } else {
            warnings.add("Property 'rootClassProvided' is not a boolean value.");
        }

        return valid;
    }

    private boolean parsePageSize(List<String> warnings) {
        String pageSize = this.properties.getProperty("defaultPageSize");

        boolean valid = StringUtility.stringHasValue(pageSize) && PATTERN.matcher(pageSize).matches();

        if (valid) {
            this.defaultPageSize = pageSize;
        } else {
            warnings.add("Property 'defaultPageSize' is not an integer number.");
        }

        return valid;
    }

    @Override
    public boolean modelExampleClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        topLevelClass.setSuperClass(rootClass + "<Criteria>");
        topLevelClass.addImportedType(rootClass);
        topLevelClass.addImportedType(rootClass + ".AbstractCriteria");
        topLevelClass.addImportedType(introspectedTable.getExampleType() + ".Criteria");
        return super.modelExampleClassGenerated(topLevelClass, introspectedTable);
    }

    @Override
    public boolean sqlMapSelectByExampleWithBLOBsElementGenerated(XmlElement answer, IntrospectedTable introspectedTable) {
        addPaginationSupportXmlElement(answer, introspectedTable);
        return true;
    }

    @Override
    public boolean sqlMapDocumentGenerated(Document document, IntrospectedTable introspectedTable) {
        super.sqlMapDocumentGenerated(document, introspectedTable);

        if (introspectedTable.getRules().generateSelectByPrimaryKey()) {
            addSelectByPrimaryKeyForUpdateXmlElement(document.getRootElement(), introspectedTable);
        }

        return true;
    }

    /**
     * 增加SelectByPrimaryKeyForUpdate的XML定义
     *
     * @param parentElement
     * @param introspectedTable
     */
    protected void addSelectByPrimaryKeyForUpdateXmlElement(XmlElement parentElement, IntrospectedTable introspectedTable) {
        SelectByPrimaryKeyForUpdateElementGenerator elementGenerator = new SelectByPrimaryKeyForUpdateElementGenerator();
        elementGenerator.setContext(context);
        elementGenerator.setIntrospectedTable(introspectedTable);
        elementGenerator.addElements(parentElement);
    }

    /**
     * 增加支持分页的SQL定义
     *
     * @param answer
     * @param introspectedTable
     */
    abstract protected void addPaginationSupportXmlElement(XmlElement answer, IntrospectedTable introspectedTable);

    @Override
    public boolean sqlMapSelectByExampleWithoutBLOBsElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
        return sqlMapSelectByExampleWithBLOBsElementGenerated(element, introspectedTable);
    }

    @Override
    public boolean clientGenerated(Interface interfaze, TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        if (introspectedTable.getRules().generateSelectByPrimaryKey()) {
            AbstractJavaMapperMethodGenerator methodGenerator = new SelectByPrimaryKeyForUpdateMethodGenerator(true);
            methodGenerator.setContext(context);
            methodGenerator.setIntrospectedTable(introspectedTable);
            methodGenerator.addInterfaceElements(interfaze);
        }
        return true;
    }

    @Override
    public List<GeneratedJavaFile> contextGenerateAdditionalJavaFiles() {
        List<GeneratedJavaFile> answer = new ArrayList<GeneratedJavaFile>();

        if (!rootClassProvided) {
            GeneratedJavaFile gjf = new GeneratedJavaFile(
                    getCompilationUnit(),
                    context.getJavaModelGeneratorConfiguration().getTargetProject(),
                    context.getProperty(PropertyRegistry.CONTEXT_JAVA_FILE_ENCODING),
                    context.getJavaFormatter()) {

                        @Override
                        public String getFileName() {
                            return classSimpleName + ".java"; //$NON-NLS-1$
                        }

                    };
            answer.add(gjf);
        }

        return answer;
    }

    private CompilationUnit getCompilationUnit() {
        FullyQualifiedJavaType type = new FullyQualifiedJavaType(rootClass + "<T extends AbstractCriteria>");
        TopLevelClass topLevelClass = new TopLevelClass(type);
        topLevelClass.setVisibility(JavaVisibility.PUBLIC);
        topLevelClass.setAbstract(true);
        topLevelClass.addImportedType(rootClass + ".AbstractCriteria");
        topLevelClass.addImportedType("java.util.ArrayList");
        topLevelClass.addImportedType("java.util.List");
        topLevelClass.addImportedType("java.lang.reflect.Array");
        topLevelClass.addImportedType("java.util.Collection");
        topLevelClass.addImportedType("java.util.Map");

        // add default constructor
        Method method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setConstructor(true);
        method.setName(classSimpleName);
        method.addBodyLine("oredCriteria = new ArrayList<T>();"); //$NON-NLS-1$
        topLevelClass.addMethod(method);

        // add field, getter, setter for pageSize
        Field field = new Field();
        field.setVisibility(JavaVisibility.PRIVATE);
        field.setType(FullyQualifiedJavaType.getIntInstance());
        field.setName("pageSize"); //$NON-NLS-1$
        field.setInitializationString(defaultPageSize);
        topLevelClass.addField(field);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("setPageSize"); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getIntInstance(), "pageSize")); //$NON-NLS-1$
        method.addBodyLine("this.pageSize = pageSize;"); //$NON-NLS-1$
        topLevelClass.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setReturnType(FullyQualifiedJavaType.getIntInstance());
        method.setName("getPageSize"); //$NON-NLS-1$
        method.addBodyLine("return pageSize;"); //$NON-NLS-1$
        topLevelClass.addMethod(method);

        // add field, getter, setter for pageOffset
        field = new Field();
        field.setVisibility(JavaVisibility.PRIVATE);
        field.setType(FullyQualifiedJavaType.getIntInstance());
        field.setName("pageOffset"); //$NON-NLS-1$
        field.setInitializationString("-1"); //$NON-NLS-1$
        topLevelClass.addField(field);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("setPageOffset"); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getIntInstance(), "pageOffset")); //$NON-NLS-1$
        method.addBodyLine("this.pageOffset = pageOffset;"); //$NON-NLS-1$
        topLevelClass.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setReturnType(FullyQualifiedJavaType.getIntInstance());
        method.setName("getPageOffset"); //$NON-NLS-1$
        method.addBodyLine("return pageOffset;"); //$NON-NLS-1$
        topLevelClass.addMethod(method);

        // add getter for pageFirst
        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setReturnType(FullyQualifiedJavaType.getIntInstance());
        method.setName("getPageFirst"); //$NON-NLS-1$
        method.addBodyLine("return (pageOffset * pageSize) + 1;"); //$NON-NLS-1$
        topLevelClass.addMethod(method);

        // add getter for pageLast
        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setReturnType(FullyQualifiedJavaType.getIntInstance());
        method.setName("getPageLast"); //$NON-NLS-1$
        method.addBodyLine("return this.getPageFirst() + pageSize;"); //$NON-NLS-1$
        topLevelClass.addMethod(method);

        // add field, getter, setter for orderby clause
        field = new Field();
        field.setVisibility(JavaVisibility.PRIVATE);
        field.setType(FullyQualifiedJavaType.getStringInstance());
        field.setName("orderByClause"); //$NON-NLS-1$
        topLevelClass.addField(field);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("setOrderByClause"); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "orderByClause")); //$NON-NLS-1$
        method.addBodyLine("this.orderByClause = orderByClause;"); //$NON-NLS-1$
        topLevelClass.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setReturnType(FullyQualifiedJavaType.getStringInstance());
        method.setName("getOrderByClause"); //$NON-NLS-1$
        method.addBodyLine("return orderByClause;"); //$NON-NLS-1$
        topLevelClass.addMethod(method);

        // add field, getter, setter for distinct
        field = new Field();
        field.setVisibility(JavaVisibility.PRIVATE);
        field.setType(FullyQualifiedJavaType.getBooleanPrimitiveInstance());
        field.setName("distinct"); //$NON-NLS-1$
        topLevelClass.addField(field);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("setDistinct"); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getBooleanPrimitiveInstance(), "distinct")); //$NON-NLS-1$
        method.addBodyLine("this.distinct = distinct;"); //$NON-NLS-1$
        topLevelClass.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setReturnType(FullyQualifiedJavaType.getBooleanPrimitiveInstance());
        method.setName("isDistinct"); //$NON-NLS-1$
        method.addBodyLine("return distinct;"); //$NON-NLS-1$
        topLevelClass.addMethod(method);

        // add field, getter, setter for orderby clause
        field = new Field();
        field.setVisibility(JavaVisibility.PRIVATE);
        field.setType(FullyQualifiedJavaType.getStringInstance());
        field.setName("columns"); //$NON-NLS-1$
        topLevelClass.addField(field);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("setColumns"); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "columns")); //$NON-NLS-1$
        method.addBodyLine("this.columns = columns;"); //$NON-NLS-1$
        topLevelClass.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setReturnType(FullyQualifiedJavaType.getStringInstance());
        method.setName("getColumns"); //$NON-NLS-1$
        method.addBodyLine("return columns;"); //$NON-NLS-1$
        topLevelClass.addMethod(method);

        // add field and methods for the list of ored criteria
        field = new Field();
        field.setVisibility(JavaVisibility.PRIVATE);

        FullyQualifiedJavaType fqjt = new FullyQualifiedJavaType("java.util.List<T>"); //$NON-NLS-1$
        field.setType(fqjt);
        field.setName("oredCriteria"); //$NON-NLS-1$
        topLevelClass.addField(field);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setReturnType(fqjt);
        method.setName("getOredCriteria"); //$NON-NLS-1$
        method.addBodyLine("return oredCriteria;"); //$NON-NLS-1$
        topLevelClass.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("or"); //$NON-NLS-1$
        method.addParameter(new Parameter(new FullyQualifiedJavaType("T"), "criteria")); //$NON-NLS-1$
        method.addBodyLine("oredCriteria.add(criteria);"); //$NON-NLS-1$
        topLevelClass.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("or"); //$NON-NLS-1$
        method.setReturnType(new FullyQualifiedJavaType("T"));
        method.addBodyLine("T criteria = createCriteriaInternal();"); //$NON-NLS-1$
        method.addBodyLine("oredCriteria.add(criteria);"); //$NON-NLS-1$
        method.addBodyLine("return criteria;"); //$NON-NLS-1$
        topLevelClass.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("createCriteria"); //$NON-NLS-1$
        method.setReturnType(new FullyQualifiedJavaType("T"));
        method.addBodyLine("T criteria = createCriteriaInternal();"); //$NON-NLS-1$
        method.addBodyLine("if (oredCriteria.isEmpty()) {"); //$NON-NLS-1$
        method.addBodyLine("oredCriteria.add(criteria);"); //$NON-NLS-1$
        method.addBodyLine("}"); //$NON-NLS-1$
        method.addBodyLine("return criteria;"); //$NON-NLS-1$
        topLevelClass.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PROTECTED);
        method.setName("createCriteriaInternal"); //$NON-NLS-1$
        method.setReturnType(new FullyQualifiedJavaType("T"));
        topLevelClass.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("getColumnMapping"); //$NON-NLS-1$
        method.setReturnType(new FullyQualifiedJavaType("Map<String, String>"));
        topLevelClass.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("clear"); //$NON-NLS-1$
        method.addBodyLine("oredCriteria.clear();"); //$NON-NLS-1$
        method.addBodyLine("orderByClause = null;"); //$NON-NLS-1$
        method.addBodyLine("distinct = false;"); //$NON-NLS-1$
        topLevelClass.addMethod(method);

        topLevelClass.addInnerClass(getCriteriaAssertionInnerClass());
        topLevelClass.addInnerClass(getCharSequenceCriteriaAssertionInnerClass());
        topLevelClass.addInnerClass(getAbstractCriteriaInnerClass());
        topLevelClass.addInnerClass(getCriterionInnerClass());

        return topLevelClass;
    }

    private InnerClass getCriteriaAssertionInnerClass() {
        InnerClass answer = new InnerClass(new FullyQualifiedJavaType("CriteriaAssertion<CRITERIA extends AbstractCriteria, INPUT_TYPE>")); //$NON-NLS-1$
        answer.setVisibility(JavaVisibility.PUBLIC);
        answer.setStatic(true);

        FullyQualifiedJavaType typeOfCriteria = new FullyQualifiedJavaType("CRITERIA");
        FullyQualifiedJavaType typeOfInputType = new FullyQualifiedJavaType("INPUT_TYPE");

        Field field = new Field();
        field.setName("criteria"); //$NON-NLS-1$
        field.setType(typeOfCriteria);
        field.setVisibility(JavaVisibility.PRIVATE);
        field.setFinal(true);
        answer.addField(field);
        answer.addMethod(getGetter(field));

        field = new Field();
        field.setName("property"); //$NON-NLS-1$
        field.setType(FullyQualifiedJavaType.getStringInstance());
        field.setVisibility(JavaVisibility.PRIVATE);
        field.setFinal(true);
        answer.addField(field);
        answer.addMethod(getGetter(field));

        field = new Field();
        field.setName("column"); //$NON-NLS-1$
        field.setType(FullyQualifiedJavaType.getStringInstance());
        field.setVisibility(JavaVisibility.PRIVATE);
        field.setFinal(true);
        answer.addField(field);
        answer.addMethod(getGetter(field));

        field = new Field();
        field.setName("typeHandler"); //$NON-NLS-1$
        field.setType(FullyQualifiedJavaType.getStringInstance());
        field.setVisibility(JavaVisibility.PRIVATE);
        field.setFinal(true);
        answer.addField(field);
        answer.addMethod(getGetter(field));

        Method method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("CriteriaAssertion"); //$NON-NLS-1$
        method.setConstructor(true);
        method.addParameter(new Parameter(typeOfCriteria, "criteria")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "column")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "property")); //$NON-NLS-1$
        method.addBodyLine("this(criteria, column, property, null);"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("CriteriaAssertion"); //$NON-NLS-1$
        method.setConstructor(true);
        method.addParameter(new Parameter(typeOfCriteria, "criteria")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "column")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "property")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "typeHandler")); //$NON-NLS-1$
        method.addBodyLine("this.criteria = criteria;"); //$NON-NLS-1$
        method.addBodyLine("this.column = column;"); //$NON-NLS-1$
        method.addBodyLine("this.property = property;"); //$NON-NLS-1$
        method.addBodyLine("this.typeHandler = typeHandler;"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("isNull"); //$NON-NLS-1$
        method.setReturnType(typeOfCriteria);
        method.addBodyLine("criteria.addCriterion(column + \" is null\");"); //$NON-NLS-1$
        method.addBodyLine("return criteria;"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("isNotNull"); //$NON-NLS-1$
        method.setReturnType(typeOfCriteria);
        method.addBodyLine("criteria.addCriterion(column + \" is not null\");"); //$NON-NLS-1$
        method.addBodyLine("return criteria;"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("equalTo"); //$NON-NLS-1$
        method.setReturnType(typeOfCriteria);
        method.addParameter(new Parameter(typeOfInputType, "value")); //$NON-NLS-1$
        method.addBodyLine("assertNotBlank(value);"); //$NON-NLS-1$
        method.addBodyLine("criteria.addCriterion(column + \" = \", value, property, typeHandler);"); //$NON-NLS-1$
        method.addBodyLine("return criteria;"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("notEqualTo"); //$NON-NLS-1$
        method.setReturnType(typeOfCriteria);
        method.addParameter(new Parameter(typeOfInputType, "value")); //$NON-NLS-1$
        method.addBodyLine("assertNotBlank(value);"); //$NON-NLS-1$
        method.addBodyLine("criteria.addCriterion(column + \" <> \", value, property, typeHandler);"); //$NON-NLS-1$
        method.addBodyLine("return criteria;"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("greaterThan"); //$NON-NLS-1$
        method.setReturnType(typeOfCriteria);
        method.addParameter(new Parameter(typeOfInputType, "value")); //$NON-NLS-1$
        method.addBodyLine("assertNotBlank(value);"); //$NON-NLS-1$
        method.addBodyLine("criteria.addCriterion(column + \" > \", value, property, typeHandler);"); //$NON-NLS-1$
        method.addBodyLine("return criteria;"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("greaterThanOrEqualTo"); //$NON-NLS-1$
        method.setReturnType(typeOfCriteria);
        method.addParameter(new Parameter(typeOfInputType, "value")); //$NON-NLS-1$
        method.addBodyLine("assertNotBlank(value);"); //$NON-NLS-1$
        method.addBodyLine("criteria.addCriterion(column + \" >= \", value, property, typeHandler);"); //$NON-NLS-1$
        method.addBodyLine("return criteria;"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("lessThan"); //$NON-NLS-1$
        method.setReturnType(typeOfCriteria);
        method.addParameter(new Parameter(typeOfInputType, "value")); //$NON-NLS-1$
        method.addBodyLine("assertNotBlank(value);"); //$NON-NLS-1$
        method.addBodyLine("criteria.addCriterion(column + \" < \", value, property, typeHandler);"); //$NON-NLS-1$
        method.addBodyLine("return criteria;"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("lessThanOrEqualTo"); //$NON-NLS-1$
        method.setReturnType(typeOfCriteria);
        method.addParameter(new Parameter(typeOfInputType, "value")); //$NON-NLS-1$
        method.addBodyLine("assertNotBlank(value);"); //$NON-NLS-1$
        method.addBodyLine("criteria.addCriterion(column + \" <= \", value, property, typeHandler);"); //$NON-NLS-1$
        method.addBodyLine("return criteria;"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("in"); //$NON-NLS-1$
        method.setReturnType(typeOfCriteria);
        method.addParameter(new Parameter(new FullyQualifiedJavaType("List<INPUT_TYPE>"), "values")); //$NON-NLS-1$
        method.addBodyLine("assertNotBlank(values);"); //$NON-NLS-1$
        method.addBodyLine("if (values.size() == 1) {"); //$NON-NLS-1$
        method.addBodyLine("return equalTo(values.get(0));"); //$NON-NLS-1$
        method.addBodyLine("}"); //$NON-NLS-1$
        method.addBodyLine("criteria.addCriterion(column + \" in \", values, property, typeHandler);"); //$NON-NLS-1$
        method.addBodyLine("return criteria;"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("notIn"); //$NON-NLS-1$
        method.setReturnType(typeOfCriteria);
        method.addParameter(new Parameter(new FullyQualifiedJavaType("List<INPUT_TYPE>"), "values")); //$NON-NLS-1$
        method.addBodyLine("assertNotBlank(values);"); //$NON-NLS-1$
        method.addBodyLine("if (values.size() == 1) {"); //$NON-NLS-1$
        method.addBodyLine("return notEqualTo(values.get(0));"); //$NON-NLS-1$
        method.addBodyLine("}"); //$NON-NLS-1$
        method.addBodyLine("criteria.addCriterion(column + \" not in \", values, property, typeHandler);"); //$NON-NLS-1$
        method.addBodyLine("return criteria;"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("between"); //$NON-NLS-1$
        method.setReturnType(typeOfCriteria);
        method.addParameter(new Parameter(typeOfInputType, "value1")); //$NON-NLS-1$
        method.addParameter(new Parameter(typeOfInputType, "value2")); //$NON-NLS-1$
        method.addBodyLine("assertNotBlank(value1);"); //$NON-NLS-1$
        method.addBodyLine("assertNotBlank(value2);"); //$NON-NLS-1$
        method.addBodyLine("criteria.addCriterion(column + \" between \", value1, value2, property, typeHandler);"); //$NON-NLS-1$
        method.addBodyLine("return criteria;"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("notBetween"); //$NON-NLS-1$
        method.setReturnType(typeOfCriteria);
        method.addParameter(new Parameter(typeOfInputType, "value1")); //$NON-NLS-1$
        method.addParameter(new Parameter(typeOfInputType, "value2")); //$NON-NLS-1$
        method.addBodyLine("assertNotBlank(value1);"); //$NON-NLS-1$
        method.addBodyLine("assertNotBlank(value2);"); //$NON-NLS-1$
        method.addBodyLine("criteria.addCriterion(column + \" not between \", value1, value2, property, typeHandler);"); //$NON-NLS-1$
        method.addBodyLine("return criteria;"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PROTECTED);
        method.setName("assertNotBlank"); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getObjectInstance(), "value")); //$NON-NLS-1$
        method.addBodyLine("boolean isBlank = (value == null);"); //$NON-NLS-1$
        method.addBodyLine("if (value != null) {"); //$NON-NLS-1$
        method.addBodyLine("if (String.class.isAssignableFrom(value.getClass())) {"); //$NON-NLS-1$
        method.addBodyLine("isBlank = ((String) value).length() == 0;"); //$NON-NLS-1$
        method.addBodyLine("} else if (value.getClass().isArray()) {"); //$NON-NLS-1$
        method.addBodyLine("isBlank = (Array.getLength(value) == 0);"); //$NON-NLS-1$
        method.addBodyLine("} else if (Collection.class.isAssignableFrom(value.getClass())) {"); //$NON-NLS-1$
        method.addBodyLine("isBlank = (((Collection) value).isEmpty());"); //$NON-NLS-1$
        method.addBodyLine("}"); //$NON-NLS-1$
        method.addBodyLine("}"); //$NON-NLS-1$
        method.addBodyLine("if (isBlank) {"); //$NON-NLS-1$
        method.addBodyLine("throw new RuntimeException(\"Value for \" + property + \" cannot be null or empty.\");"); //$NON-NLS-1$
        method.addBodyLine("}"); //$NON-NLS-1$
        answer.addMethod(method);

        return answer;
    }

    private InnerClass getCharSequenceCriteriaAssertionInnerClass() {
        InnerClass answer = new InnerClass(new FullyQualifiedJavaType("CharSequenceCriteriaAssertion<CRITERIA extends AbstractCriteria>")); //$NON-NLS-1$
        answer.setVisibility(JavaVisibility.PUBLIC);
        answer.setSuperClass(new FullyQualifiedJavaType("CriteriaAssertion<CRITERIA, CharSequence>"));
        answer.setStatic(true);

        FullyQualifiedJavaType typeOfCriteria = new FullyQualifiedJavaType("CRITERIA");
        FullyQualifiedJavaType typeOfInputType = new FullyQualifiedJavaType("java.lang.CharSequence");

        Method method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("CharSequenceCriteriaAssertion"); //$NON-NLS-1$
        method.setConstructor(true);
        method.addParameter(new Parameter(typeOfCriteria, "criteria")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "column")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "property")); //$NON-NLS-1$
        method.addBodyLine("super(criteria, column, property);"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("CharSequenceCriteriaAssertion"); //$NON-NLS-1$
        method.setConstructor(true);
        method.addParameter(new Parameter(typeOfCriteria, "criteria")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "column")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "property")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "typeHandler")); //$NON-NLS-1$
        method.addBodyLine("super(criteria, column, property, typeHandler);"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("like"); //$NON-NLS-1$
        method.setReturnType(typeOfCriteria);
        method.addParameter(new Parameter(typeOfInputType, "value")); //$NON-NLS-1$
        method.addBodyLine("assertNotBlank(value);"); //$NON-NLS-1$
        method.addBodyLine("getCriteria().addCriterion(getColumn() + \" like '%\" + value + \"%'\");"); //$NON-NLS-1$
        method.addBodyLine("return getCriteria();"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("startWith"); //$NON-NLS-1$
        method.setReturnType(typeOfCriteria);
        method.addParameter(new Parameter(typeOfInputType, "value")); //$NON-NLS-1$
        method.addBodyLine("assertNotBlank(value);"); //$NON-NLS-1$
        method.addBodyLine("getCriteria().addCriterion(getColumn() + \" like '\" + value + \"%'\");"); //$NON-NLS-1$
        method.addBodyLine("return getCriteria();"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("endWith"); //$NON-NLS-1$
        method.setReturnType(typeOfCriteria);
        method.addParameter(new Parameter(typeOfInputType, "value")); //$NON-NLS-1$
        method.addBodyLine("assertNotBlank(value);"); //$NON-NLS-1$
        method.addBodyLine("getCriteria().addCriterion(getColumn() + \" like '%\" + value + \"'\");"); //$NON-NLS-1$
        method.addBodyLine("return getCriteria();"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("notLike"); //$NON-NLS-1$
        method.setReturnType(typeOfCriteria);
        method.addParameter(new Parameter(typeOfInputType, "value")); //$NON-NLS-1$
        method.addBodyLine("assertNotBlank(value);"); //$NON-NLS-1$
        method.addBodyLine("getCriteria().addCriterion(getColumn() + \" not like '%\" + value + \"%'\");"); //$NON-NLS-1$
        method.addBodyLine("return getCriteria();"); //$NON-NLS-1$
        answer.addMethod(method);

        return answer;
    }

    private InnerClass getCriterionInnerClass() {
        InnerClass answer = new InnerClass(new FullyQualifiedJavaType("Criterion")); //$NON-NLS-1$
        answer.setVisibility(JavaVisibility.PUBLIC);
        answer.setStatic(true);

        Field field = new Field();
        field.setName("condition"); //$NON-NLS-1$
        field.setType(FullyQualifiedJavaType.getStringInstance());
        field.setVisibility(JavaVisibility.PRIVATE);
        answer.addField(field);
        answer.addMethod(getGetter(field));

        field = new Field();
        field.setName("value"); //$NON-NLS-1$
        field.setType(FullyQualifiedJavaType.getObjectInstance());
        field.setVisibility(JavaVisibility.PRIVATE);
        answer.addField(field);
        answer.addMethod(getGetter(field));

        field = new Field();
        field.setName("secondValue"); //$NON-NLS-1$
        field.setType(FullyQualifiedJavaType.getObjectInstance());
        field.setVisibility(JavaVisibility.PRIVATE);
        answer.addField(field);
        answer.addMethod(getGetter(field));

        field = new Field();
        field.setName("noValue"); //$NON-NLS-1$
        field.setType(FullyQualifiedJavaType.getBooleanPrimitiveInstance());
        field.setVisibility(JavaVisibility.PRIVATE);
        answer.addField(field);
        answer.addMethod(getGetter(field));

        field = new Field();
        field.setName("singleValue"); //$NON-NLS-1$
        field.setType(FullyQualifiedJavaType.getBooleanPrimitiveInstance());
        field.setVisibility(JavaVisibility.PRIVATE);
        answer.addField(field);
        answer.addMethod(getGetter(field));

        field = new Field();
        field.setName("betweenValue"); //$NON-NLS-1$
        field.setType(FullyQualifiedJavaType.getBooleanPrimitiveInstance());
        field.setVisibility(JavaVisibility.PRIVATE);
        answer.addField(field);
        answer.addMethod(getGetter(field));

        field = new Field();
        field.setName("listValue"); //$NON-NLS-1$
        field.setType(FullyQualifiedJavaType.getBooleanPrimitiveInstance());
        field.setVisibility(JavaVisibility.PRIVATE);
        answer.addField(field);
        answer.addMethod(getGetter(field));

        field = new Field();
        field.setName("typeHandler"); //$NON-NLS-1$
        field.setType(FullyQualifiedJavaType.getStringInstance());
        field.setVisibility(JavaVisibility.PRIVATE);
        answer.addField(field);
        answer.addMethod(getGetter(field));

        Method method = new Method();
        method.setVisibility(JavaVisibility.PROTECTED);
        method.setName("Criterion"); //$NON-NLS-1$
        method.setConstructor(true);
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "condition")); //$NON-NLS-1$
        method.addBodyLine("super();"); //$NON-NLS-1$
        method.addBodyLine("this.condition = condition;"); //$NON-NLS-1$
        method.addBodyLine("this.typeHandler = null;"); //$NON-NLS-1$
        method.addBodyLine("this.noValue = true;"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PROTECTED);
        method.setName("Criterion"); //$NON-NLS-1$
        method.setConstructor(true);
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "condition")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getObjectInstance(), "value")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "typeHandler")); //$NON-NLS-1$
        method.addBodyLine("super();"); //$NON-NLS-1$
        method.addBodyLine("this.condition = condition;"); //$NON-NLS-1$
        method.addBodyLine("this.value = value;"); //$NON-NLS-1$
        method.addBodyLine("this.typeHandler = typeHandler;"); //$NON-NLS-1$
        method.addBodyLine("if (value instanceof List<?>) {"); //$NON-NLS-1$
        method.addBodyLine("this.listValue = true;"); //$NON-NLS-1$
        method.addBodyLine("} else {"); //$NON-NLS-1$
        method.addBodyLine("this.singleValue = true;"); //$NON-NLS-1$
        method.addBodyLine("}"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PROTECTED);
        method.setName("Criterion"); //$NON-NLS-1$
        method.setConstructor(true);
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "condition")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getObjectInstance(), "value")); //$NON-NLS-1$
        method.addBodyLine("this(condition, value, null);"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PROTECTED);
        method.setName("Criterion"); //$NON-NLS-1$
        method.setConstructor(true);
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "condition")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getObjectInstance(), "value")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getObjectInstance(), "secondValue")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "typeHandler")); //$NON-NLS-1$
        method.addBodyLine("super();"); //$NON-NLS-1$
        method.addBodyLine("this.condition = condition;"); //$NON-NLS-1$
        method.addBodyLine("this.value = value;"); //$NON-NLS-1$
        method.addBodyLine("this.secondValue = secondValue;"); //$NON-NLS-1$
        method.addBodyLine("this.typeHandler = typeHandler;"); //$NON-NLS-1$
        method.addBodyLine("this.betweenValue = true;"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PROTECTED);
        method.setName("Criterion"); //$NON-NLS-1$
        method.setConstructor(true);
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "condition")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getObjectInstance(), "value")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getObjectInstance(), "secondValue")); //$NON-NLS-1$
        method.addBodyLine("this(condition, value, secondValue, null);"); //$NON-NLS-1$
        answer.addMethod(method);

        return answer;
    }

    private InnerClass getAbstractCriteriaInnerClass() {
        InnerClass answer = new InnerClass(new FullyQualifiedJavaType("AbstractCriteria"));

        answer.setVisibility(JavaVisibility.PUBLIC);
        answer.setStatic(true);

        Method method = new Method();
        method.setVisibility(JavaVisibility.PROTECTED);
        method.setName("AbstractCriteria"); //$NON-NLS-1$
        method.setConstructor(true);
        method.addBodyLine("super();"); //$NON-NLS-1$
        method.addBodyLine("criteria = new ArrayList<Criterion>();"); //$NON-NLS-1$
        method.addBodyLine("allCriteria = new ArrayList<Criterion>();"); //$NON-NLS-1$
        answer.addMethod(method);

        // now generate the isValid method
        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("isValid"); //$NON-NLS-1$
        method.setReturnType(FullyQualifiedJavaType.getBooleanPrimitiveInstance());
        method.addBodyLine("return !getAllCriteria().isEmpty();");
        answer.addMethod(method);

        // now generate the criteria field and getter
        Field field = new Field();
        field.setName("criteria"); //$NON-NLS-1$
        field.setFinal(true);
        field.setType(new FullyQualifiedJavaType("List<Criterion>")); //$NON-NLS-1$
        field.setVisibility(JavaVisibility.PRIVATE);
        answer.addField(field);
        answer.addMethod(getGetter(field));


        // now generate the criteria field and getter
        field = new Field();
        field.setName("allCriteria"); //$NON-NLS-1$
        field.setFinal(true);
        field.setType(new FullyQualifiedJavaType("List<Criterion>")); //$NON-NLS-1$
        field.setVisibility(JavaVisibility.PRIVATE);
        answer.addField(field);

        method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName("getAllCriteria"); //$NON-NLS-1$
        method.setReturnType(new FullyQualifiedJavaType("java.util.List<Criterion>")); //$NON-NLS-1$
        method.addBodyLine("allCriteria.clear();"); //$NON-NLS-1$
        method.addBodyLine("allCriteria.addAll(criteria);"); //$NON-NLS-1$
        method.addBodyLine("return allCriteria;"); //$NON-NLS-1$
        answer.addMethod(method);

        // now add the methods for simplifying the individual field set methods
        method = new Method();
        method.setVisibility(JavaVisibility.PROTECTED);
        method.setName("addCriterion"); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "condition")); //$NON-NLS-1$
        method.addBodyLine("if (condition == null) {"); //$NON-NLS-1$
        method.addBodyLine("throw new RuntimeException(\"Value for condition cannot be null\");"); //$NON-NLS-1$
        method.addBodyLine("}"); //$NON-NLS-1$
        method.addBodyLine("criteria.add(new Criterion(condition));"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PROTECTED);
        method.setName("addCriterion"); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "condition")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getObjectInstance(), "value")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "property")); //$NON-NLS-1$
        method.addBodyLine("addCriterion(condition, value, property, null);"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PROTECTED);
        method.setName("addCriterion"); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "condition")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getObjectInstance(), "value")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "property")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "typeHandler")); //$NON-NLS-1$
        method.addBodyLine("if (value == null) {"); //$NON-NLS-1$
        method.addBodyLine("throw new RuntimeException(\"Value for \" + property + \" cannot be null\");"); //$NON-NLS-1$
        method.addBodyLine("}"); //$NON-NLS-1$
        method.addBodyLine("getCriteria(typeHandler).add(new Criterion(condition, value, typeHandler));"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PROTECTED);
        method.setName("addCriterion"); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "condition")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getObjectInstance(), "value1")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getObjectInstance(), "value2")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "property")); //$NON-NLS-1$
        method.addBodyLine("addCriterion(condition, value1, value2, property, null);"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PROTECTED);
        method.setName("addCriterion"); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "condition")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getObjectInstance(), "value1")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getObjectInstance(), "value2")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "property")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "typeHandler")); //$NON-NLS-1$
        method.addBodyLine("if (value1 == null || value2 == null) {"); //$NON-NLS-1$
        method.addBodyLine("throw new RuntimeException(\"Between values for \" + property + \" cannot be null\");"); //$NON-NLS-1$
        method.addBodyLine("}"); //$NON-NLS-1$
        method.addBodyLine("getCriteria(typeHandler).add(new Criterion(condition, value1, value2, typeHandler));"); //$NON-NLS-1$
        answer.addMethod(method);

        method = new Method();
        method.setVisibility(JavaVisibility.PROTECTED);
        method.setName("getCriteria"); //$NON-NLS-1$
        method.setReturnType(new FullyQualifiedJavaType("java.util.List<Criterion>")); //$NON-NLS-1$
        method.addParameter(new Parameter(FullyQualifiedJavaType.getStringInstance(), "typeHandler")); //$NON-NLS-1$
        method.addBodyLine("return criteria;"); //$NON-NLS-1$
        answer.addMethod(method);

        return answer;
    }

    private static final char PACKAGE_SEPARATOR_CHAR = '.';
    private static final char INNER_CLASS_SEPARATOR_CHAR = '$';

    private static String getShortClassName(String className, String defaultShortClassName) {
        if (!StringUtility.stringHasValue(className)) {
            return defaultShortClassName;
        }

        int lastDotIdx = className.lastIndexOf(PACKAGE_SEPARATOR_CHAR);
        int innerIdx = className.indexOf(INNER_CLASS_SEPARATOR_CHAR, lastDotIdx == -1 ? 0 : lastDotIdx + 1);
        String out = className.substring(lastDotIdx + 1);
        if (innerIdx != -1) {
            out = out.replace(INNER_CLASS_SEPARATOR_CHAR, PACKAGE_SEPARATOR_CHAR);
        }
        return out;
    }
}
