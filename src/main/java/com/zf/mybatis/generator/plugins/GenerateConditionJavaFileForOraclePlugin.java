package com.zf.mybatis.generator.plugins;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;

/**
 * 用于Oracle生成分页语句
 *
 * @author zhifeng
 */
public class GenerateConditionJavaFileForOraclePlugin extends GenerateConditionJavaFilePlugin {

    @Override
    protected void addPaginationSupportXmlElement(XmlElement answer, IntrospectedTable introspectedTable) {
        addBefore(answer);
        addAfter(answer);
    }

    private void addBefore(XmlElement answer) {
        XmlElement ifElement = new XmlElement("if");
        ifElement.addAttribute(TEST_ATTRIBUTE_WITHIN_PAGE);
        ifElement.addElement(new TextElement("SELECT * FROM (SELECT PAGINATE.*, ROWNUM ROWNO FROM ("));
        answer.addElement(0, ifElement);
    }

    private void addAfter(XmlElement answer) {
        XmlElement ifElement = new XmlElement("if");
        ifElement.addAttribute(TEST_ATTRIBUTE_WITHIN_PAGE);
        ifElement.addElement(new TextElement("<![CDATA[\n        ) PAGINATE WHERE ROWNUM < #{pageLast,jdbcType=INTEGER} ) WHERE ROWNO >= #{pageFirst,jdbcType=INTEGER}\n      ]]>"));
        answer.addElement(ifElement);
    }
}
